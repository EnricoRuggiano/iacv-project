cmake_minimum_required(VERSION 2.8)
project(IACV-project)

# Add source files
file(GLOB_RECURSE SOURCE_FILES
	${CMAKE_SOURCE_DIR}/src/src/*.cpp)

set(HEADER_FILES
	${CMAKE_SOURCE_DIR}/src/include/calibProc
  ${CMAKE_SOURCE_DIR}/src/include/Multithreading
	${CMAKE_SOURCE_DIR}/src/include/utils
	${CMAKE_SOURCE_DIR}/src/include)

# Pthread
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")

# get OpenCV
find_package( OpenCV REQUIRED )

# headers
include_directories( 
	${OpenCV_INCLUDE_DIRS}
	 ${HEADER_FILES})

# bin
add_executable(${PROJECT_NAME} ${SOURCE_FILES})

# link 
target_link_libraries(IACV-project ${OpenCV_LIBS} )