#include "string"
#include <stdio.h>
#include "video_streaming.h"
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

// get the video streaming from an url
int play_streaming(std::string & url)
{
  // get video from url
  cv::VideoCapture capture(url);
  if(!capture.isOpened())
  {
  	// error
  	fprintf(stderr, "error - opening video source %s\n", url.c_str());
  	return -1;
  }
  
  // open a window
  cv::namedWindow("VideoStreaming", cv::WINDOW_AUTOSIZE);
  cv::Mat frame;
  
  // while cycle
  while(true)
  {
  	if(!capture.read(frame))
  	{
  	  // Error
  	  fprintf(stderr, "error - capturing video frame\n");
  	  return -1;
  	}
  	
  	// do stuff here ...
  	
  	cv::imshow("VideoStreaming", frame);
  	
  	cv::waitKey(30);
  }
  
  return 0;
} 