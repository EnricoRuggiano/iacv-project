#include "OCVLogger.h"
#include <stdarg.h>
#include <stdio.h>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <string>


void OCVLogger::Log(OCVLogger::Priority logLevel, const char * fmt, va_list args)
{
  // get the time
  std::time_t rawTime = std::time(nullptr);
  std::stringstream ss;
  ss << std::put_time(std::localtime(&rawTime), "%y-%m-%d %OH:%OM:%OS");
  std::string s = ss.str();

  // get log level
  std::string strLogLevel;
  switch(logLevel)
  {
    case DEBUG_LEVEL:
      strLogLevel = "DEBUG";
      break;
    case INFO_LEVEL:
      strLogLevel = "INFO";
      break;
    case WARN_LEVEL:
      strLogLevel = "WARN";
      break;
    case ERROR_LEVEL:
      strLogLevel = "ERROR";
      break;
    default:
      strLogLevel = "DEBUG";
      break;
  }

  // arrange format string
  std::string strFormat = s + " " + strLogLevel + " " + fmt + "\n";
  
  // print message
  vfprintf(stderr, strFormat.c_str(), args);
}

void OCVLogger::Debug(const char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  OCVLogger::Log(OCVLogger::Priority::DEBUG_LEVEL, fmt, args);
  va_end(args);
}

void OCVLogger::Info(const char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  OCVLogger::Log(OCVLogger::Priority::DEBUG_LEVEL, fmt, args);
  va_end(args);
}

void OCVLogger::Warn(const char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  OCVLogger::Log(OCVLogger::Priority::DEBUG_LEVEL, fmt, args);
  va_end(args);
}

void OCVLogger::Error(const char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  OCVLogger::Log(OCVLogger::Priority::DEBUG_LEVEL, fmt, args);
  va_end(args);
}
