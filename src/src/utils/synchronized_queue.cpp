#include "synchronized_queue.h"
#include <functional>
#include <vector>
#include <opencv2/core.hpp>

template <typename T>
void SynchronizedQueue<T>::push(const T & elem)
{
  std::unique_lock<std::mutex> l(mMutex);
  // critical start
  mList.push_back(elem);
  mConditionVariable.notify_one();
  // critical end
}

template <typename T>
unsigned int SynchronizedQueue<T>::size()
{
//  mMutex.lock();
  unsigned int size = (unsigned int)mList.size();
//  mMutex.unlock();
  return size;
}

template <typename T>
T SynchronizedQueue<T>::get()
{
  std::unique_lock<std::mutex> l(mMutex);

  while(mList.empty())
  {
    mConditionVariable.wait(l);
  }
  T result = mList.front();
  mList.pop_front();
  return result;
}

template class SynchronizedQueue<int>;
template class SynchronizedQueue<std::function <void ()>>;
template class SynchronizedQueue<std::vector<cv::Point2f>>;
