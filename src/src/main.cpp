#include "calibProc.h"
#include "synchronized_queue.h"
#include <string>

#include "Processor.h"
#include <vector>
#include <opencv2/core.hpp>

int main()
{
  std::string configPath("../config/cameraConfig_imageList.xml");
  Processor processor(configPath);
  processor.run();
  return 0;
}
