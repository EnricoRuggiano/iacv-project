#include "calibData.h"
#include <opencv2/calib3d.hpp>

CalibData::CalibData() : goodInput(false)
{
#ifdef BBQUE
  logger = bbque::utils::Logger::GetLogger("opencvcameracalibration");
  logger->Info("Bbque logger is binded to Camera Processor");
#else
  logger = std::unique_ptr<OCVLogger>(new OCVLogger);
  logger->Info("CalibProcessor logger is created correctly");
#endif
}

void CalibData::write(cv::FileStorage &fs) const //Write serialization for this class
{
  fs << "{"
     << "BoardSize_Width" << boardSize.width
     << "BoardSize_Height" << boardSize.height
     << "ImageSize_Width" << imageSize.width
     << "ImageSize_Height" << imageSize.height
     << "Square_Size" << squareSize
     << "Calibrate_Pattern" << patternToUse
     << "Calibrate_NrOfFrameToUse" << nrFrames
     << "Calibrate_FixAspectRatio" << aspectRatio
     << "Calibrate_AssumeZeroTangentialDistortion" << calibZeroTangentDist
     << "Calibrate_FixPrincipalPointAtTheCenter" << calibFixPrincipalPoint

     << "Write_DetectedFeaturePoints" << writePoints
     << "Write_extrinsicParameters" << writeExtrinsics
     << "Write_gridPoints" << writeGrid
     << "Write_outputFileName" << outputFileName

     << "Show_UndistortedImage" << showUndistorsed

     << "Input_FlipAroundHorizontalAxis" << flipVertical
     << "Input_Delay" << delay
     << "Input" << input
     << "}";
}

void CalibData::read(const cv::FileNode &node) //Read serialization for this class
{
  node["BoardSize_Width"] >> boardSize.width;
  node["BoardSize_Height"] >> boardSize.height;
  node["ImageSize_Width"] >> imageSize.width;
  node["ImageSize_Height"] >> imageSize.height;
  node["Calibrate_Pattern"] >> patternToUse;
  node["Square_Size"] >> squareSize;
  node["Calibrate_NrOfFrameToUse"] >> nrFrames;
  node["Calibrate_FixAspectRatio"] >> aspectRatio;
  node["Write_DetectedFeaturePoints"] >> writePoints;
  node["Write_extrinsicParameters"] >> writeExtrinsics;
  node["Write_gridPoints"] >> writeGrid;
  node["Write_outputFileName"] >> outputFileName;
  node["Calibrate_AssumeZeroTangentialDistortion"] >> calibZeroTangentDist;
  node["Calibrate_FixPrincipalPointAtTheCenter"] >> calibFixPrincipalPoint;
  node["Calibrate_UseFisheyeModel"] >> useFisheye;
  node["Input_FlipAroundHorizontalAxis"] >> flipVertical;
  node["Show_UndistortedImage"] >> showUndistorsed;
  node["Input"] >> input;
  node["Input_Delay"] >> delay;
  node["Fix_K1"] >> fixK1;
  node["Fix_K2"] >> fixK2;
  node["Fix_K3"] >> fixK3;
  node["Fix_K4"] >> fixK4;
  node["Fix_K5"] >> fixK5;
  node["win_size"] >> winSize;

  validate();
}

void CalibData::validate()
{
  goodInput = true;
  if (boardSize.width <= 0 || boardSize.height <= 0)
  {
    logger->Info("Calib Data - Invalid board size with width: %f height: %f", boardSize.width, boardSize.height);
    ////cerr << "Invalid Board size: " << boardSize.width << " " << boardSize.height << endl;
    goodInput = false;
  }
  if (squareSize <= 10e-6)
  {
    logger->Info("Calib Data - Invalid square size: %f", squareSize);
    //cerr << "Invalid square size " << squareSize << endl;
    goodInput = false;
  }
  if (nrFrames <= 0)
  {
    logger->Info("Calib Data - Invalid number of frames: %d", nrFrames);
    //cerr << "Invalid number of frames " << nrFrames << endl;
    goodInput = false;
  }

  if (input.empty())
  {
    // Check for valid input
    inputType = INVALID;
    logger->Info("Calib Data - Empty input value");
  }
  else
  {
    if (input[0] >= '0' && input[0] <= '9')
    {
      std::stringstream ss(input);
      ss >> cameraID;
      inputType = CAMERA;
      logger->Info("Calib Data - input type CAMERA with id: %d", cameraID);
      inputCapture.open(cameraID);
      if (!inputCapture.isOpened())
      {
        logger->Info("Calib Data - Impossible to open CAMERA %d", cameraID);
        inputType = INVALID;
      }
    }
    else if (isListOfImages(input) && readStringList(input, imageList))
    {
      inputType = IMAGE_LIST;
      nrFrames = (nrFrames < (int)imageList.size()) ? nrFrames : (int)imageList.size();
      logger->Info("Calib Data - input type IMAGE_LIST of %d", nrFrames);
    }
    else
    {
      inputType = VIDEO_FILE;
      logger->Info("Calib Data - input type VIDEO_FILE");

      inputCapture.open(input);
      if (!inputCapture.isOpened())
      {
        logger->Info("Calib Data - Impossible to open VIDEO_FILE %s", input.c_str());
        inputType = INVALID;
      }
    }
  }

  if (inputType == INVALID)
  {
    logger->Info("Calib Data - input type is INVALID");
    goodInput = false;
  }

  flag = 0;
  if (calibFixPrincipalPoint)
  {
    logger->Info("Calib Data - setting fix principal point flag");
    flag |= cv::CALIB_FIX_PRINCIPAL_POINT;
  }

  if (calibZeroTangentDist)
  {
    logger->Info("Calib Data - setting zero tangent distance flag");
    flag |= cv::CALIB_ZERO_TANGENT_DIST;
  }
  if (aspectRatio)
  {
    logger->Info("Calib Data - setting fix aspect ratio flag");
    flag |= cv::CALIB_FIX_ASPECT_RATIO;
  }
  if (fixK1)
  {
    logger->Info("Calib Data - setting fix k1 flag");
    flag |= cv::CALIB_FIX_K1;
  }
  if (fixK2)
  {
    logger->Info("Calib Data - setting fix k2 flag");
    flag |= cv::CALIB_FIX_K2;
  }
  if (fixK3)
  {
    logger->Info("Calib Data - setting fix k3 flag");
    flag |= cv::CALIB_FIX_K3;
  }
  if (fixK4)
  {
    logger->Info("Calib Data - setting fix k4 flag");
    flag |= cv::CALIB_FIX_K4;
  }
  if (fixK5)
  {
    logger->Info("Calib Data - setting fix k5 flag");
    flag |= cv::CALIB_FIX_K5;
  }

  if (useFisheye)
  {
    // the fisheye model has its own enum, so overwrite the flags
    logger->Info("Calib Data - fisheye model ");
    flag = cv::fisheye::CALIB_FIX_SKEW | cv::fisheye::CALIB_RECOMPUTE_EXTRINSIC;
    if (fixK1)
    {
      flag |= cv::fisheye::CALIB_FIX_K1;
      logger->Info("Calib Data - setting fisheye model fix k1 flag");
    }

    if (fixK2)
    {
      flag |= cv::fisheye::CALIB_FIX_K2;
      logger->Info("Calib Data - setting fisheye model fix k2 flag");
    }
    if (fixK3)
    {
      flag |= cv::fisheye::CALIB_FIX_K3;
      logger->Info("Calib Data - setting fisheye model fix k3 flag");
    }
    if (fixK4)
    {
      flag |= cv::fisheye::CALIB_FIX_K4;
      logger->Info("Calib Data - setting fisheye model fix k4 flag");
    }
    if (calibFixPrincipalPoint)
    {
      flag |= cv::fisheye::CALIB_FIX_PRINCIPAL_POINT;
      logger->Info("Calib Data - setting fisheye model fix principal point flag");
    }
  }

  calibrationPattern = NOT_EXISTING;
  if (!patternToUse.compare("CHESSBOARD"))
  {
    calibrationPattern = Pattern::CHESSBOARD;

    logger->Info("Calib Data - pattern CHESSBOARD");
  }
  if (!patternToUse.compare("CIRCLES_GRID"))
  {
    calibrationPattern = Pattern::CIRCLES_GRID;
    logger->Info("Calib Data - pattern CIRCLES GRID");
  }

  if (!patternToUse.compare("ASYMMETRIC_CIRCLES_GRID"))
  {
    calibrationPattern = Pattern::ASYMMETRIC_CIRCLES_GRID;
    logger->Info("Calib Data - pattern ASYMMETRIC CIRCLES GRID");
  }
  if (calibrationPattern == Pattern::NOT_EXISTING)
  {
    logger->Info("Calib Data - pattern NOT EXISTING");
    goodInput = false;
  }

  // others
  atImageList = 0;
  gridWidth = squareSize * (boardSize.width - 1);
  release_object = false;
  winSize = 11;
}

bool CalibData::readStringList(const std::string &filename, std::vector<std::string> &l)
{
  l.clear();
  cv::FileStorage fs(filename, cv::FileStorage::READ);
  if (!fs.isOpened())
  {
    logger->Info("CalibData - %s not found", filename.c_str());
    return false;
  }
  cv::FileNode n = fs.getFirstTopLevelNode();
  if (n.type() != cv::FileNode::SEQ)
  {
    logger->Info("CalibData - %s is not a sequence file", filename.c_str());
    return false;
  }
  cv::FileNodeIterator it = n.begin();
  cv::FileNodeIterator it_end = n.end();
  for (; it != it_end; ++it)
  {
    l.push_back((std::string)*it);
  }
  return true;
}

bool CalibData::isListOfImages(const std::string &filename)
{
  std::string s(filename);
  // Look for file extension
  if (
      s.find(".xml") == std::string::npos &&
      s.find(".yaml") == std::string::npos &&
      s.find(".yml") == std::string::npos)
  {
    logger->Info("CalibData - Image List file %s is not a parsable config file", s.c_str());
    return false;
  }
  else
  {
    return true;
  }
}

int CalibData::loadConfigFile(std::string &configFilePath)
{
  /*
  // open the file
  cv::FileStorage configFile = cv::FileStorage(configFilePath, cv::FileStorage::READ);

  if (!configFile.isOpened())
  {
    // default params
    return -1;
  }

  configFile["Settings"] >> this;
  configFile.release(); // close Settings file

  //FileStorage fout("settings.yml", FileStorage::WRITE); // write config as YAML
  //fout << "Settings" << s;

  if (!goodInput)
  {
    return -1;
  }

  // input name
  /*  delay = (int)configFile["delay"];
  nrFrames = (int)configFile["nrFrames"];

  //board
  boardWidth = (float)configFile["boardWidth"];

  // parse input
  inputName = inputName;
  outputName = outputName;
  gridWidth = gridWidth;

  // specific use case
  boardSize.height = 5;
  boardSize.width = 4;
  delay = 50;
  squareSize = 50;

  winSize = 11;

  inputName = "/dev/video0";
  inputMode = VIDEO;
  pattern = CHESSBOARD;

  if (inputMode == VIDEO)
  {
    inputCapture.open(inputName);
  }
  */

  return 0;
}

cv::Mat CalibData::nextImage()
{
  cv::Mat result;
  if (inputCapture.isOpened())
  {
    cv::Mat view0;
    inputCapture >> view0;
    view0.copyTo(result);
  }
  else if (atImageList < imageList.size())
  {
    logger->Info("CalibData - get image num %d", atImageList);
    result = cv::imread(imageList[atImageList++], cv::IMREAD_COLOR);
  }
  return result;
}