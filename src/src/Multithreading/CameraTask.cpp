#include <string>
#include <sstream>
#include <cstdio>
#include <vector>
#include <memory>
#include <ctime>
#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include "CameraTask.h"
#include "synchronized_queue.h"

void sampleData(
    SynchronizedQueue<std::vector<cv::Point2f>> &imagePoints, CalibData &calibData)
{
  // init the sampling of data
  CameraTask::Mode mode = CameraTask::CAPTURING;
  calibData.logger->Info("Sample Data - Starting to sample the data");

  if (!calibData.goodInput)
  {
    calibData.logger->Error("Sample Data - calib data is not valid. Aborting Sampling Frames...");
    return;
  }

  std::clock_t prevTimestamp = 0;
  const cv::Scalar RED(0, 0, 255), GREEN(0, 255, 0);
  const char ESC_KEY = 27;
  int i = 0;

  for (;;)
  {
    // sample the image
    cv::Mat view = calibData.nextImage();
    bool blinkOutput = false;

    // stop if num of frames is reached
    if (i >= (size_t)calibData.nrFrames)
    {
      calibData.logger->Info("Sample Data - Limit of %d frames reached. Stop Sampling", calibData.nrFrames);
      mode = CameraTask::CALIBRATED;
      break;
    }

    // data
    //calibData.imageSize = view.size();
    //calibData.logger->Info("Sample Data - image size %d %d", calibData.imageSize)
    std::vector<cv::Point2f> pointBuf;
    bool found;
    int chessBoardFlags = cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_NORMALIZE_IMAGE;

    // flip if it is set
    if (calibData.flipVertical)
    {
      flip(view, view, 0);
    }

    // set flag if fisheye is set
    if (!calibData.useFisheye)
    {
      // fast check erroneously fails with high distortions like fisheye
      chessBoardFlags |= cv::CALIB_CB_FAST_CHECK;
    }

    // extract from pattern
    switch (calibData.calibrationPattern) // Find feature points on the input format
    {
    case CalibData::CHESSBOARD:
    {
      found = cv::findChessboardCorners(view, calibData.boardSize, pointBuf, chessBoardFlags);
      calibData.logger->Info("Sample Data - %d]frame sample Cheesboard: %d", i, found);
      break;
    }
    case CalibData::CIRCLES_GRID:
    {
      found = cv::findCirclesGrid(view, calibData.boardSize, pointBuf);
      calibData.logger->Info("Sample Data - %d]frame sample Circles: %d", i, found);
      break;
    }
    case CalibData::ASYMMETRIC_CIRCLES_GRID:
    {
      found = cv::findCirclesGrid(view, calibData.boardSize, pointBuf, cv::CALIB_CB_ASYMMETRIC_GRID);
      calibData.logger->Info("Sample Data - %d]frame sample Asymmetric Circles: %d", i, found);
      break;
    }
    default:
    {
      calibData.logger->Error("Sample Data - Calibration Pattern not supported");
      found = false;
      break;
    }
    }

    // if found data
    if (found)
    {

      // here we have some optimization ...
      if (calibData.calibrationPattern == CalibData::CHESSBOARD)
      {
        cv::Mat viewGray;
        cv::cvtColor(view, viewGray, cv::COLOR_BGR2GRAY);
        cv::cornerSubPix(
            viewGray,
            pointBuf,
            cv::Size(calibData.winSize, calibData.winSize),
            cv::Size(-1, -1),
            cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT,
                             30,
                             0.0001));
      }

      if (mode == CameraTask::CAPTURING && // For camera only take new samples after delay time
          (!calibData.inputCapture.isOpened() ||
           std::clock() - prevTimestamp > calibData.delay * 1e-3 * CLOCKS_PER_SEC))
      {
        //imagePoints.push_back(pointBuf);
        imagePoints.push(pointBuf);
        prevTimestamp = std::clock();
        blinkOutput = calibData.inputCapture.isOpened();

        // print image points
        std::stringstream ss;

        ss << pointBuf;
        std::string s = ss.str();
        calibData.logger->Info("Sample Data - add image points: \n%s", s.c_str());
      }

      // Draw the corners.
      cv::drawChessboardCorners(view, calibData.boardSize, cv::Mat(pointBuf), found);
    }

    // Fancy OpenCV debug
    std::string msg = "Calibrated";
    int baseLine = 0;
    cv::Size textSize = cv::getTextSize(msg, 1, 1, 1, &baseLine);
    cv::Point textOrigin(view.cols - 2 * textSize.width - 10, view.rows - 2 * baseLine - 10);

    if (calibData.showUndistorsed)
    {
      msg = cv::format("Capturing: %d/%d Undist", (int)imagePoints.size(), calibData.nrFrames);
    }
    else
    {
      msg = cv::format("Capturing: %d/%d", (int)imagePoints.size(), calibData.nrFrames);
    }

    cv::putText(view, msg, textOrigin, 1, 1, mode == CameraTask::CALIBRATED ? GREEN : RED);

    if (blinkOutput)
    {
      cv::bitwise_not(view, view);
    }

    if (mode == CameraTask::CALIBRATED && calibData.showUndistorsed)
    {
      cv::Mat temp = view.clone();
      if (calibData.useFisheye)
      {
        //cv::fisheye::undistortImage(temp, view, cameraMatrix, distCoeffs);
      }
      else
      {
        //undistort(temp, view, cameraMatrix, distCoeffs);
      }
    }

    // await input
    cv::imshow("Image View", view);
    char key = (char)cv::waitKey(calibData.inputCapture.isOpened() ? 50 : calibData.delay);

    // stop if user kill process with esc key
    if (key == ESC_KEY)
    {
      calibData.logger->Info("Sample Data - exiting from capturing");
      break;
    }

    i++;

    // --------------------------------------------
    // break loop condition
    // --------------------------------------------

    if (view.empty()) // If there are no more images stop the loop
    {
      calibData.logger->Info("Sample Data - view is empty, exiting from infinite loop");
      break;
    }
  }

  calibData.logger->Info("Sample Data - ending with sample data");
}

// Run Calibration
void runCalibration(
    SynchronizedQueue<std::vector<cv::Point2f>> &queue,
    CalibData &calibData)
{
  // data structure for calibrations
  cv::Mat cameraMatrix;
  cv::Mat distCoeffs;
  std::vector<cv::Mat> rvecs, tvecs;
  std::vector<float> reprojErrs;
  double totalAvgErr = 0;
  std::vector<cv::Point3f> newObjPoints;
  float rms;

  // image points (empty)
  std::vector<std::vector<cv::Point2f>> imagePoints;

  // calibrate
  calibData.logger->Info("Sample Data - starting calibration");

  // infinite loop
  for (;;)
  {
    if (queue.size() > 0)
    {
      // calibrate
      calibData.logger->Info("Sample Data - get new image points");

      // add to imagePoints the new coming from queue
      imagePoints.push_back(queue.get());

      if (!calibData.goodInput)
      {
        calibData.logger->Error("Sample Data -calib data is not valid. Aborting Calibration...");
      }

      //std::vector<cv::Mat> rvecs, tvecs;
      //std::vector<float> reprojErrs;
      //double totalAvgErr = 0;
      //std::vector<cv::Point3f> newObjPoints;

      cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
      if (calibData.flag & cv::CALIB_FIX_ASPECT_RATIO)
      {
        cameraMatrix.at<double>(0, 0) = calibData.aspectRatio;
        calibData.logger->Info("Calib Proc - Calibration with fixed aspect Ratio %f", calibData.aspectRatio);
      }

      if (calibData.useFisheye)
      {
        distCoeffs = cv::Mat::zeros(4, 1, CV_64F);
        calibData.logger->Info("Calib Proc - Calibration with fisheye model");
      }

      else
      {
        distCoeffs = cv::Mat::zeros(8, 1, CV_64F);
      }

      std::vector<std::vector<cv::Point3f>> objectPoints(1);
      // calculate board corner positions
      objectPoints[0].resize(0);

      switch (calibData.calibrationPattern)
      {
      case CalibData::CHESSBOARD:
      case CalibData::CIRCLES_GRID:
        for (int i = 0; i < calibData.boardSize.height; ++i)
          for (int j = 0; j < calibData.boardSize.width; ++j)
            objectPoints[0].push_back(cv::Point3f(float(j * calibData.squareSize),
                                                  float(i * calibData.squareSize), 0));
        break;

      case CalibData::ASYMMETRIC_CIRCLES_GRID:
        for (int i = 0; i < calibData.boardSize.height; i++)
          for (int j = 0; j < calibData.boardSize.width; j++)
            objectPoints[0].push_back(cv::Point3f(float((2 * j + i % 2) * calibData.squareSize),
                                                  float(i * calibData.squareSize), 0));
        break;
      default:
        break;
      }

      objectPoints[0][calibData.boardSize.width - 1].x = objectPoints[0][0].x + calibData.gridWidth;
      newObjPoints = objectPoints[0];

      objectPoints.resize(imagePoints.size(), objectPoints[0]);

      //Find intrinsic and extrinsic camera parameters
      //double rms;

      if (calibData.useFisheye)
      {
        cv::Mat _rvecs, _tvecs;
        rms = cv::fisheye::calibrate(
            objectPoints,
            imagePoints,
            calibData.imageSize,
            cameraMatrix,
            distCoeffs,
            _rvecs,
            _tvecs,
            calibData.flag);

        rvecs.reserve(_rvecs.rows);
        tvecs.reserve(_tvecs.rows);
        for (int i = 0; i < int(objectPoints.size()); i++)
        {
          rvecs.push_back(_rvecs.row(i));
          tvecs.push_back(_tvecs.row(i));
        }
      }
      else
      {
        int iFixedPoint = -1;
        if (calibData.release_object)
        {
          iFixedPoint = calibData.boardSize.width - 1;
        }

        // debug ROI
        std::stringstream ss;
        std::string s;

        for (int i = 0; i < objectPoints.size(); i++)
        {
          ss << objectPoints[i];
          s = ss.str();
          ss.str("");
          calibData.logger->Info("objectPoints\n %s", s.c_str());
        }

        calibData.logger->Info("imagePoints\n ");
        for (int i = 0; i < imagePoints.size(); i++)
        {
          calibData.logger->Info("%d]\n ", i);
          for (int j = 0; j < imagePoints[i].size(); j++)
          {
            ss << imagePoints[i][j];
            s = ss.str();
            calibData.logger->Info("%s \t", s.c_str());
            ss.str("");
          }
        }

        ss << calibData.imageSize;
        s = ss.str();
        ss.str("");
        calibData.logger->Info("imageSize\n %s", s.c_str());

        ss << iFixedPoint;
        s = ss.str();
        ss.str("");
        calibData.logger->Info("iFixedPoint\n %s", s.c_str());

        ss << cameraMatrix;
        s = ss.str();
        ss.str("");
        calibData.logger->Info("cameraMatrix\n %s", s.c_str());

        ss << distCoeffs;
        s = ss.str();
        ss.str("");
        calibData.logger->Info("distCoeffs\n %s", s.c_str());

        ss << newObjPoints;
        s = ss.str();
        ss.str("");
        calibData.logger->Info("newObjPoints\n %s", s.c_str());

        ss << calibData.flag;
        s = ss.str();
        ss.str("");
        calibData.logger->Info("flag\n %s", s.c_str());

        rms = cv::calibrateCameraRO(
            objectPoints,
            imagePoints,
            calibData.imageSize,
            iFixedPoint,
            cameraMatrix,
            distCoeffs,
            rvecs,
            tvecs,
            newObjPoints,
            calibData.flag | cv::CALIB_USE_LU);
      }
      std::stringstream ss;

      if (calibData.release_object)
      {
        ss << newObjPoints[0] << std::endl;
        ss << newObjPoints[calibData.boardSize.width - 1] << std::endl;
        ss << newObjPoints[calibData.boardSize.width * (calibData.boardSize.height - 1)] << std::endl;
        ss << newObjPoints.back();
        std::string s = ss.str();
        calibData.logger->Info("Run Calibration - release object - New board corners:\n %s", s.c_str());
        ss.clear();
      }

      // reprojection error
      calibData.logger->Info("Run Calibration - Re-projection error %f", rms);

      bool ok = cv::checkRange(cameraMatrix) && cv::checkRange(distCoeffs);

      objectPoints.clear();
      objectPoints.resize(imagePoints.size(), newObjPoints);
      totalAvgErr = computeReprojectionErrors(
          objectPoints,
          imagePoints,
          rvecs,
          tvecs,
          cameraMatrix,
          distCoeffs,
          reprojErrs,
          calibData.useFisheye);

      calibData.logger->Info("Run Calibration - Total Average Error %f", totalAvgErr);

      // the output
      std::string s;

      ss << cameraMatrix;
      s = ss.str();
      calibData.logger->Info("Run Calibration - Camera Matrix: \n%s", s.c_str());
      ss.str("");
      ss << distCoeffs;
      s = ss.str();
      calibData.logger->Info("Run Calibration - dist coefficients: \n%s", s.c_str());
      ss.flush();
      
      /*
  ss << rvecs;
  s = ss.str();
  calibData.logger->Info("Run Calibration - rvecs coefficients: %s", s.c_str());
  ss.clear();
  ss << tvecs;
  s = ss.str();
  calibData.logger->Info("Run Calibration - tvecs coefficients: %s", s.c_str());
  ss.clear();
*/
    }
  }
}

double computeReprojectionErrors(
    const std::vector<std::vector<cv::Point3f>> & objectPoints,
    const std::vector<std::vector<cv::Point2f>> & imagePoints,
    const std::vector<cv::Mat> & rvecs,
    const std::vector<cv::Mat> & tvecs,
    const cv::Mat & cameraMatrix,
    const cv::Mat & distCoeffs,
    std::vector<float> &perViewErrors,
    bool fisheye)
{
  std::vector<cv::Point2f> imagePoints2;
  size_t totalPoints = 0;
  double totalErr = 0, err;
  perViewErrors.resize(objectPoints.size());

  for (size_t i = 0; i < objectPoints.size(); ++i)
  {
    if (fisheye)
    {
      cv::fisheye::projectPoints(objectPoints[i], imagePoints2, rvecs[i], tvecs[i], cameraMatrix,
                                 distCoeffs);
    }
    else
    {
      cv::projectPoints(objectPoints[i], rvecs[i], tvecs[i], cameraMatrix, distCoeffs, imagePoints2);
    }
    err = norm(imagePoints[i], imagePoints2, cv::NORM_L2);

    size_t n = objectPoints[i].size();
    perViewErrors[i] = (float)std::sqrt(err * err / n);
    totalErr += err * err;
    totalPoints += n;
  }

  return std::sqrt(totalErr / totalPoints);
}

static inline void read(const cv::FileNode &node, CalibData &x, const CalibData &default_value = CalibData())
{
  if (node.empty())
  {
    //x = default_value;
  }
  else
  {
    x.read(node);
  }
}

void CameraTask::setup(
    std::string &configFileName)
{
  // open the file
  cv::FileStorage configFile = cv::FileStorage(configFileName, cv::FileStorage::READ);

  if (!configFile.isOpened())
  {
    // default params
    calibData.logger->Info("Setup - config file %s not found", configFileName.c_str());
    return;
  }

  calibData.logger->Info("Setup - config file %s found", configFileName.c_str());
  configFile["Settings"] >> calibData;
  configFile.release(); // close Settings file

  if (!calibData.goodInput)
  {
    calibData.logger->Info("Setup - Calibration data not initialized ");
    return;
  }

  calibData.logger->Info("Setup - Calibration data correctly initialized ");
  return;
}

void CameraTask::writeResult()
{
  // get result from calib data
  // ...

  // write somewhere the output
  // ...
}