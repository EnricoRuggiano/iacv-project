#include "Processor.h"

// constructor
Processor::Processor(std::string configPath)
{
  // create the producer data
  producer.setup(configPath);

  // create the consumer data
  consumer.setup(configPath);
}

// run
void Processor::run()
{
  std::thread tProducer(sampleData, std::ref(queue), std::ref(producer.calibData));
  std::thread tConsumer(runCalibration, std::ref(queue), std::ref(consumer.calibData));
  tProducer.join();
  tConsumer.join();
}