// process the frames

#ifndef PROCESSOR_H
#define PROCESSOR_H

#include "CameraTask.h"
#include "calibData.h"
#include "synchronized_queue.h"
#include <opencv2/core.hpp>
#include <thread>
#include <functional>
#include <string>

class Processor
{
  public:
    Processor(std::string configPath);  // parse config file
    void run();

  private:
    SynchronizedQueue <std::vector<cv::Point2f>>queue;
    CalibData calibData;

    CameraTask producer;  // run only calibration
    CameraTask consumer;  // run only sampling
    
    std::thread tProducer; // threadpool ??
    std::thread tConsumer;
};

#endif