#ifndef CAMERATASK_H
#define CAMERATASK_H

#include "calibData.h"
#include <memory>
#include <vector>
#include <opencv2/core.hpp>
#include "synchronized_queue.h"

// Camera task which it is passed to a thread
class CameraTask
{
  public:
    // setup
    void setup(std::string & settingFileName);
        
    // write result
    void writeResult(); 
  
    enum Mode {DETECTION, CAPTURING, CALIBRATED};
    CalibData calibData;

  private:
    Mode mode = CAPTURING;
};


// OpenCv Functions
// sample data
 void sampleData(SynchronizedQueue <std::vector<cv::Point2f>> & imagePoints, CalibData & calibData);
 
 // run Calibration
 void runCalibration(SynchronizedQueue<std::vector<cv::Point2f>> &queue, CalibData &calibData);   

// compute Error
double computeReprojectionErrors(
    const std::vector<std::vector<cv::Point3f>> & objectPoints,
    const std::vector<std::vector<cv::Point2f>> & imagePoints,
    const std::vector<cv::Mat> & rvecs,
    const std::vector<cv::Mat> & tvecs,
    const cv::Mat & cameraMatrix,
    const cv::Mat & distCoeffs,
    std::vector<float> &perViewErrors,
    bool fisheye);

#endif