#ifndef LOGGER_H
#define LOGGER_H

class Logger
{
  public:
    virtual void Debug(const char *fmt, ...) = 0;
	  virtual void Info(const char *fmt, ...) = 0;
    virtual void Warn(const char *fmt, ...) = 0;
    virtual void Error(const char *fmt, ...) = 0;

    virtual ~Logger() {};
};

#endif