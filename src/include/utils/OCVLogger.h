#ifndef OCVLOGGER_H
#define OCVLOGGER_H

#include "logger.h"
#include <stdarg.h>

class OCVLogger: public Logger
{
  public:
    enum Priority
    {
      DEBUG_LEVEL,
		  INFO_LEVEL,
		  WARN_LEVEL,
		  ERROR_LEVEL
    };

    void Log(Priority logLevel, const char * fmt, va_list args);

    virtual void Debug(const char *fmt, ...);
	  virtual void Info(const char *fmt, ...);
    virtual void Warn(const char *fmt, ...);
    virtual void Error(const char *fmt, ...);
};

#endif