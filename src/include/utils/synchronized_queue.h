#ifndef SYNCHRONIZED_QUEUE_H
#define SYNCHRONIZED_QUEUE_H
#include <list>
#include <condition_variable>
#include <mutex>

template <typename T>
class SynchronizedQueue
{
  public:
    T get();
    void push(const T & elem);
    unsigned int size();
  private:
    std::condition_variable mConditionVariable;
    std::mutex mMutex;
    std::list <T> mList;
};
#endif
