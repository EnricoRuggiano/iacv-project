#ifndef CAMERADATA_H
#define CAMERADATA_H

#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/imgcodecs.hpp>
#include <string>
#include <vector>
#include <memory>

#ifdef BBQUE
#include <bbque/utils/logging/logger.h>
#else
#include "OCVLogger.h"
#endif


class CalibData
{
  public:

    CalibData();
    enum Pattern { CHESSBOARD, CIRCLES_GRID, ASYMMETRIC_CIRCLES_GRID, NOT_EXISTING };
    enum InputType { CAMERA, VIDEO_FILE, IMAGE_LIST, INVALID }; 

    void write(cv::FileStorage& fs) const;                        //Write serialization for this class
    void read(const cv::FileNode& node);                          //Read serialization for this class
    void validate();
    bool readStringList( const std::string& filename, std::vector<std::string>& l );
    bool isListOfImages( const std::string& filename);

    cv::Mat nextImage();
    
    // read config file and init settings
    int loadConfigFile(std::string & configFilePath);

public:

    cv::Size boardSize;          // The size of the board -> Number of items by width and height
    Pattern calibrationPattern;  // One of the Chessboard, circles, or asymmetric circle pattern
    float squareSize;            // The size of a square in your defined unit (point, millimeter,etc).
    int nrFrames;                // The number of frames to use from the input for calibration
    float aspectRatio;           // The aspect ratio
    int delay;                   // In case of a video input
    bool writePoints;            // Write detected feature points
    bool writeExtrinsics;        // Write extrinsic parameters
    bool writeGrid;              // Write refined 3D target grid points
    bool calibZeroTangentDist;   // Assume zero tangential distortion
    bool calibFixPrincipalPoint; // Fix the principal point at the center
    bool flipVertical;           // Flip the captured images around the horizontal axis
    std::string outputFileName;  // The name of the file where to write
    bool showUndistorsed;        // Show undistorted images after calibration
    std::string input;           // The input ->
    bool useFisheye;             // use fisheye camera model for calibration
    bool fixK1;                  // fix K1 distortion coefficient
    bool fixK2;                  // fix K2 distortion coefficient
    bool fixK3;                  // fix K3 distortion coefficient
    bool fixK4;                  // fix K4 distortion coefficient
    bool fixK5;                  // fix K5 distortion coefficient

    bool goodInput;
    int cameraID;
    InputType inputType;
    int flag;

    std::vector<std::string> imageList;
    size_t atImageList;
    cv::VideoCapture inputCapture;
    
    cv::Size imageSize;
    int winSize;
    float gridWidth;
    bool release_object;

    #ifdef BBQUE
    std::unique_ptr<bbque::utils::Logger> logger;
    #else 
    std::unique_ptr<OCVLogger> logger;
    #endif   

  private:
    std::string patternToUse;    // string of the pattern to use
    cv::Mat view;
   
  };


#endif