// #ifndef CAMERACALIB_H
// #define CAMERACALIB_H

// #include "calibData.h"
// #include <memory>

// /*
// #ifdef BBQUE
// #include <bbque/utils/logging/logger.h>
// #else
// #include "OCVLogger.h"
// #endif
// */
// #include <memory>
// #include <vector>
// #include <opencv2/core.hpp>

// class CalibProc
// {
//   public:
//     int sampleData();
//     int runCalibration();
//     int writeResult(); 
//     int setup(std::string & settingFileName);

//     enum Mode {DETECTION, CAPTURING, CALIBRATED};
//     std::vector<std::vector<cv::Point2f>> imagePoints;
    
//     cv::Mat cameraMatrix;
//     cv::Mat distCoeffs;
//     std::vector<cv::Mat> rvecs, tvecs;
//     std::vector<float> reprojErrs;
//     double totalAvgErr = 0;
//     std::vector<cv::Point3f> newObjPoints;
//     float rms;


//   private:
//     CalibData calibData;
//     Mode mode = CAPTURING;
// };

// #endif